import ForgeUI, {
  render,
  Fragment,
  Text,
  Heading,
  Strong,
  Avatar,
  User,
  Link,
  GlobalSettings,
  useEffect,
  useState,
  Table,
  Row,
  Cell,
  Head,
  Button,
  ButtonSet,
  Badge,
  ModalDialog,
  Form,
  StatusLozenge,
} from "@forge/ui";
import api, { route } from "@forge/api";

function getDateMonthsBefore(date, nofMonths) {
  var thisMonth = date.getMonth();
  date.setMonth(thisMonth - nofMonths);
  if (thisMonth - nofMonths < 0 && date.getMonth() != thisMonth + nofMonths) {
    date.setDate(0);
  } else if (
    thisMonth - nofMonths >= 0 &&
    date.getMonth() != thisMonth - nofMonths
  ) {
    date.setDate(0);
  }
  return date;
}

const timeDifference = (current, previous) => {
  var msPerMinute = 60 * 1000;
  var msPerHour = msPerMinute * 60;
  var msPerDay = msPerHour * 24;
  var msPerMonth = msPerDay * 30;
  var msPerYear = msPerDay * 365;

  var elapsed = current - previous;

  if (elapsed < msPerMinute) {
    return Math.round(elapsed / 1000) + " seconds ago";
  } else if (elapsed < msPerHour) {
    return Math.round(elapsed / msPerMinute) + " minutes ago";
  } else if (elapsed < msPerDay) {
    return Math.round(elapsed / msPerHour) + " hours ago";
  } else if (elapsed < msPerMonth) {
    return "approximately " + Math.round(elapsed / msPerDay) + " days ago";
  } else if (elapsed < msPerYear) {
    return "approximately " + Math.round(elapsed / msPerMonth) + " months ago";
  } else {
    return "approximately " + Math.round(elapsed / msPerYear) + " years ago";
  }
};

const fetchTotalViews = async (contentId) => {
  const dt = new Date();
  const response = await api
    .asUser()
    .requestConfluence(
      route`/rest/api/analytics/content/${contentId}/views?fromDate=${dt.getFullYear()}-${(
        "0" + (dt.getMonth() + 1).toString()
      ).slice(-2)}-01`
    );
  return response.json();
};

const fetchUniqueViews = async (contentId) => {
  const dt = new Date();
  const response = await api
    .asUser()
    .requestConfluence(
      route`/rest/api/analytics/content/${contentId}/viewers?fromDate=${dt.getFullYear()}-${(
        "0" + (dt.getMonth() + 1).toString()
      ).slice(-2)}-01`
    );
  return response.json();
};

const fetchData = async () => {
  const response = await api
    .asUser()
    .requestConfluence(
      route`/rest/api/content?expand=space,history.lastUpdated,operations`
    );
  let pages = await response.json();
  let result = await fetchMetaData(pages);
  result = minify(result);
  if (result.length > 10) {
    result = result.slice(0, 10);
  }
  return result;
};

const minify = (data) => {
  return data.results.map((page) => {
    return {
      id: page.id,
      title: page.title,
      total_views: page.total_views,
      unique_views: page.unique_views,
      watchers: page.watchers,
      comments: page.comments,
      activity: page.activity,
      status: page.status,
      canArchive: page.canArchive,
      space: {
        name: page.space.name,
      },
      history: {
        lastUpdated: {
          when: page.history.lastUpdated.when,
          friendlyWhen: page.history.lastUpdated.friendlyWhen,
        },
        createdBy: {
          accountId: page.history.createdBy.accountId,
        },
      },
      _links: {
        webui: (function () {
          let domain = new URL(page._links.self);
          let res =
            domain.protocol +
            "//" +
            domain.hostname +
            "/wiki" +
            page._links.webui;
          // console.log(res);
          return res;
        })(),
      },
    };
  });
};


const fetchComments = async (contentId) => {
  let date = new Date();
  let dateBeforeMonth = getDateMonthsBefore(date, 1);
  const response = await api
    .asUser()
    .requestConfluence(
      route`/rest/api/content/${contentId}/child/comment?expand=history.lastUpdated`
    );
  let results = await response.json();
  results = results.results;
  results.filter((result) => {
    return dateBeforeMonth <= new Date(result.history.lastUpdated.when);
  });
  return results;
};

const fetchWatchers = async (contentId) => {
  const response = await api
    .asUser()
    .requestConfluence(
      route`/rest/api/content/${contentId}/notification/child-created`
    );
  return response.json();
};

const fetchMetaData = async (data) => {
  let total_activity = 0;
  const val = data.results.map(async (page, id) => {
    // Get total views
    let total_views = await fetchTotalViews(page.id);
    data.results[id].total_views = total_views.count;

    // Get unique views
    let unique_views = await fetchUniqueViews(page.id);
    data.results[id].unique_views = unique_views.count;

    // Get Comments
    let totalComments = await fetchComments(page.id);
    data.results[id].comments = totalComments.length;

    // Get watchers
    let watchers = await fetchWatchers(page.id);
    data.results[id].watchers = watchers.size;

    data.results[id].activity = calculateActivity(data.results[id]);
    total_activity += data.results[id].activity;

    // Check if page can be archived
    let canArchive = false;
    for (let i = 0; i < page.operations.length; i++) {
      if (page.operations[i].operation === "archive") canArchive = true;
    }

    data.results[id].canArchive = canArchive;

    return [total_views, unique_views, totalComments, watchers, canArchive];
  });
  await Promise.all([].concat(...val));
  data.results.sort((a, b) => (a.activity > b.activity ? 1 : -1));
  return data;
};

const archivePage = async (contentId) => {
  const response = await api
    .asUser()
    .requestConfluence(route`/rest/api/content/archive`, {
      method: "POST",
      body: JSON.stringify({
        pages: [
          {
            id: contentId,
          },
        ],
      }),
    });
  await response.json();
  return response.json();
};

const deletePage = async (contentId) => {
  const response = await api
    .asUser()
    .requestConfluence(route`/rest/api/content/${contentId}`, {
      method: "DELETE",
    });

  // console.log(`Response: ${response.status} ${response.statusText}`);
  // console.log(await response.text());
  return response;
};

const calculateActivity = (data) => {
  let w1 = 1,
    w2 = 2,
    w3 = 3,
    w4 = 4,
    w5 =
      (new Date() - new Date(data.history.lastUpdated.when)) /
      (1000 * 3600 * 24);
  let activity =
    w1 * data.total_views +
    w2 * data.unique_views +
    w3 * data.comments +
    w4 * data.watchers +
    1 / w5;
  return activity;
};

const fetchCurrentUser = async () => {
  let response = await api.asUser().requestConfluence(route`/wiki/rest/api/user/current`);
  return response.json();
}

const App = () => {
  const [startTime, setStartTime] = useState(new Date());
  const [data, setData] = useState(fetchData);
  const [user, setUser] = useState(fetchCurrentUser);
  const [isDeleteOpen, setDeleteOpen] = useState(false);
  const [isOpen, setOpen] = useState(false);
  const [selectedPageIndex, setSelectedPageIndex] = useState(0);

  console.log("Response Time: " + ((new Date()) - startTime) + "ms")
  return (
    <Fragment>
    <Text>
      The table shows the pages having the least activity. (Various metrics such as views, comments, watchers in the past month are used to determine the activity of the page)
    </Text>
    <Text>‎‎‏‏‎ 　</Text>
      <Table>
        <Head>
          <Cell>
            <Text>Page Name</Text>
          </Cell>
          <Cell>
            <Text>Space Name</Text>
          </Cell>
          <Cell>
            <Text>Last Updated</Text>
          </Cell>
          <Cell>
            <Text>Status</Text>
          </Cell>
          <Cell>
            <Text>Activity</Text>
          </Cell>
          <Cell>
            <Text> </Text>
          </Cell>
        </Head>
        {data.map((page, index) => {
          if (page.activity >= threshold) return;
          return (
            <Row>
              <Cell>
                <Text>
                  <Link
                    href={page._links.webui}
                  >
                    {page.title}
                  </Link>
                </Text>
              </Cell>
              <Cell>
                <Text>{page.space.name}</Text>
              </Cell>
              <Cell>
                <Text>{page.history.lastUpdated.friendlyWhen}</Text>
              </Cell>
            <Cell>
              <Text><StatusLozenge text={page.status} appearance={page.status==="archived"?"default":"inprogress"}/></Text>
            </Cell>
            <Cell>
              <Text>
                <Badge appearance="removed" text={(
                  ((page.activity - threshold) / threshold) * 100).toFixed(1) + "% ↓"} 
                />
              </Text>
            </Cell>
            <Cell>                
              <Button
                text="Take Action"
                onClick={() => {setOpen(true);setSelectedPageIndex(index)}} />   
            </Cell>
          </Row>
        );
      })}
    </Table>
      {isOpen && (
        <ModalDialog
          header={
            "Analytics for " +
            data[selectedPageIndex].title +
            " for the last month:"
          }
          onClose={() => setOpen(false)}
        >
          <Text>
              This page's activity was <Badge appearance="removed" text={(
                  ((threshold - data[selectedPageIndex].activity) / threshold) * 100).toFixed(1) + "%"} 
                /> lower than the average activity.
          </Text>
          <Text>
            This page had
            <Strong> {data[selectedPageIndex].total_views}</Strong> total
            viewers,
            <Strong> {data[selectedPageIndex].unique_views}</Strong> unique
            viewers,
            <Strong> {data[selectedPageIndex].comments}</Strong> comments,
            <Strong> {data[selectedPageIndex].watchers}</Strong> watchers and
            was last updated{" "}
            <Strong>
              {data[selectedPageIndex].history.lastUpdated.friendlyWhen}
            </Strong>
            .
          </Text>
          <Text>
            <Strong>Created By: </Strong>
          </Text>
          <Avatar accountId={data[selectedPageIndex].history.createdBy.accountId} />
          <Text>
            <Strong>Space Name: </Strong>
            {data[selectedPageIndex].space.name}
          </Text>
          <ButtonSet>
            <Button
              text={
                data[selectedPageIndex].status === "archived"
                  ? "Archived"
                  : data[selectedPageIndex].history.createdBy.accountId !== user.accountId
                  ? "Ask owner to archive"
                  : "Archive"
              }
              disabled={
                data[selectedPageIndex].status === "archived"
                  ? true
                  : data[selectedPageIndex].history.createdBy.accountId !== user.accountId
                  ? true
                  : false
              }
              onClick={async () => {
                await archivePage(data[selectedPageIndex].id);
                let data_copy = data
                data_copy[selectedPageIndex].status="archived";
                setData(data_copy);
              }}
            />
            <Button
              text="Delete"
              appearance="danger"
              onClick={() => {
                setDeleteOpen(true);
              }}
            />
          </ButtonSet>
        </ModalDialog>
      )}
      {isDeleteOpen && (
        <ModalDialog
          header={"Deleting page " + data[selectedPageIndex].title}
          onClose={() => setDeleteOpen(false)}
          appearance="danger"
        >
          <Form
            submitButtonText="Delete"
            onSubmit={async () => {
              await deletePage(data[selectedPageIndex].id);
              let data_copy = data;
              data_copy.splice(selectedPageIndex, 1);
              setData(data_copy);
              setDeleteOpen(false);
              setOpen(false);
            }
          }
        >
          <Text>Are you sure you want to delete this page? This action cannot be reversed.</Text>
        </Form>
      </ModalDialog>
    )}
    </Fragment>
  );
};

export const run = render(
  <GlobalSettings>
    <App />
  </GlobalSettings>
);
